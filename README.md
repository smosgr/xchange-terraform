# Xchange 2021 - Terraform - Stelios Moschos - Thayna Dorneles

### How to use this repo

git clone https://gitlab.com/smosgr/xchange-terraform

Each directory is named after the steps that were followed during the presentation. Within each directory, you will find the files that
correspond to the final configuration for that specific step.

Don't forget to run a `terraform init` for each new directory you use!

### Terraform Apply Yourself! ###
#Useful commands#

- terraform plan 
- terraform apply
- terraform state list
- terraform show list
- terraform destroy <--- remember to use when you are done with a step