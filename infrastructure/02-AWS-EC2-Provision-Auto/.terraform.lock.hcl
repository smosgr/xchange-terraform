# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/aws" {
  version     = "3.44.0"
  constraints = "~> 3.27"
  hashes = [
    "h1:VOVZWybe1x0E4qyawTwt7jXVBRUplTrzVFHim217DqI=",
    "zh:0680315b29a140e9b7e4f5aeed3f2445abdfab31fc9237f34dcad06de4f410df",
    "zh:13811322a205fb4a0ee617f0ae51ec94176befdf569235d0c7064db911f0acc7",
    "zh:25e427a1cfcb1d411bc12040cf0684158d094416ecf18889a41196bacc761729",
    "zh:40cd6acd24b060823f8d116355d8f844461a11925796b1757eb2ee18abc0bc7c",
    "zh:94e2463eef555c388cd27f6e85ad803692d6d80ffa621bdc382ab119001d4de4",
    "zh:aadc3bc216b14839e85b463f07b8507920ace5f202a608e4a835df23711c8a0d",
    "zh:ab50dc1242af5a8fcdb18cf89beeaf2b2146b51ecfcecdbea033913a5f4c1c14",
    "zh:ad48bbf4af66b5d48ca07c5c558d2f5724311db4dd943c1c98a7f3f107e03311",
    "zh:ad76796c2145a7aaec1970a5244f5c0a9d200556121e2c5b382f296597b1a03c",
    "zh:cf0a2181356598f8a2abfeaf0cdf385bdeea7f2e52821c850a2a08b60c26b9f6",
    "zh:f76801af6bc34fe4a5bf1c63fa0204e24b81691049efecd6baa1526593f03935",
  ]
}

provider "registry.terraform.io/kreuzwerker/docker" {
  version = "2.12.2"
  hashes = [
    "h1:c3m3y/kVhWhlEp6KJxNu5AWdgCLNyDZVuseyN/XVE2s=",
    "zh:09cc24a0fc139d5d6b9c87f0a2524bb65f99292616b507247de6b6bb3c00574a",
    "zh:0c6514395d58b12e2c31f414aab5ff10320631125910a290f8a1adf54ea1846d",
    "zh:2233d3d86eae2a97623e6df9a39a596a81b0e7cb234546d49ab6591d3d0124e7",
    "zh:5099fd407d99d45994ceda040fd2058c78409cd6eccff36b31a0024008d12112",
    "zh:55b9d5bf30aeb90ffdd7897c5582a9065231b38e9b8b731546d6f91981987b36",
    "zh:86ad339ae1ca6120e1be8a108ef90b23e3200e4e2c8e09d48cf01af4b5298672",
    "zh:8cfefe6db6afa5330816b3de32f1f45e858166d706f3b7fb4d04d66b8052e575",
    "zh:a737a9aea2bf52ec28a7d35617e374d5dc5f0e3d448ebdce85a0cafccb515232",
    "zh:a8960217c2a0e0ee0314a1ffcd9dcb15bff8a1e13f329a3d19cd8d684e4124c1",
    "zh:d210ce15bcf8af7f02ad13f1af6742bdb276bfec0dd80dbf00349fd6a643b385",
    "zh:d37b58756b8fa1425391918b37b5793fa937d2c2ac5218cc5324a1faf588186b",
    "zh:d8cfcd231a771240f659b059871ff3838634bb6c658dd171d8d246f4b0957cee",
    "zh:e506a637f2bcdd440bf3f60b5ec07860fa01f7ab7431ba4ef2c6ad63a060523d",
  ]
}
