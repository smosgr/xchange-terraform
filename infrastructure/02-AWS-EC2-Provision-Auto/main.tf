terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }

  backend "s3" {
    bucket = "smosgr-xchange"
    key = "ec2_state_files/terraform.tfstate"
    region = "eu-west-2"
  }
}

provider "aws" {
  profile = "default"
  region  = "eu-west-2"
}


resource "aws_instance" "ec2-server" {
  ami           = "ami-03ac5a9b225e99b02"
  instance_type = "t2.micro"

  tags = {
    Name = "Ciao / Bonjour/ Hello / Hola"
  }
}
